//ТЕОРІЯ
// Це технологія , яка дозволяє надсилати та отримувати дані з вебсервера без оновлення сторінкию

//ПРАКТИКА
const API_URL = "https://ajax.test-danit.com/api/swapi/films"; 
const allEpisodes = document.getElementById('episodesList');

function getFilm () {
    fetch (API_URL, {
        method: 'GET'
    })
    .then((response) => response.json())
    .then((films) => {
        films.forEach ( film => {
            const episode = document.createElement ('li');
            episode.innerHTML = `
            <h2>${film.episodeId}: ${film.name}</h2>
            <p>${film.openingCrawl}</p>
            <div id="characters-${film.episodeId}"></div>
            `;
            allEpisodes.append(episode);
            charactersFilm(film.episodeId, film.characters);
        });
    })
};

function charactersFilm(episodeId, characters) {
    const charactersContainer = document.getElementById(`characters-${episodeId}`);
    characters.forEach(url => {
        fetch(url, {
           method: 'GET'            
       })
            .then((response) => response.json()) 
            .then(character => {                
                const characterFilm = document.createElement('div');
                characterFilm.classList = 'characterFilm';
                characterFilm.innerHTML = `${character.name}`;
                charactersContainer.append(characterFilm);
            })
    })
}  

getFilm();
